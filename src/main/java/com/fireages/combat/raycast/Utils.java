package com.fireages.combat.raycast;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.util.Vector;

public class Utils {

    public static Entity getFirst(Location loc, double acc, double distace, EntityType et, Entity custer){
        Location c = loc.clone();
        Vector dir = loc.getDirection();
        for(double i = 0; loc.distance(c) < distace; i += acc){
            c.add(dir.clone().multiply(i));
            for(Entity en : loc.getWorld().getNearbyEntities(c, acc, acc, acc)){
                if(en.getType() == et && !en.getUniqueId().equals(custer.getUniqueId())){
                    return en;
                }
            }
            if(c.getBlock().getType().isSolid()){
                break;
            }
        }
        return null;
    }

    public static Vector genVec(Location a, Location b) {
        double dX = a.getX() - b.getX();
        double dY = a.getY() - b.getY();
        double dZ = a.getZ() - b.getZ();
        double yaw = Math.atan2(dZ, dX);
        double pitch = Math.atan2(Math.sqrt(dZ * dZ + dX * dX), dY) + Math.PI;
        double x = Math.sin(pitch) * Math.cos(yaw);
        double y = Math.sin(pitch) * Math.sin(yaw);
        double z = Math.cos(pitch);

        Vector vector = new Vector(x, z, y);
        vector = vector.normalize();

        return vector;
    }
}
