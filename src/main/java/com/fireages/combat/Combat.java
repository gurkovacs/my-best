package com.fireages.combat;

import com.fireages.combat.events.CombatEvents;
import com.fireages.combat.items.BaseMelee;
import com.fireages.combat.items.BaseRange;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class Combat extends JavaPlugin {

    public static Combat instance;

    @Override
    public void onEnable() {
        instance = this;
        BaseMelee.init();
        BaseRange.init();
        Bukkit.getPluginManager().registerEvents(new CombatEvents(), this);

        getCommand("test").setExecutor(new TestCommand());
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
