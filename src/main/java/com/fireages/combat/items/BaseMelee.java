package com.fireages.combat.items;

import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class BaseMelee implements PvPItems{

    public static final String ONLY_NUMBER = "[^\\.\\d]*";
    public double damage = 0.0;
    public Slot slot;
    public double crit = 0.0;
    public double range = 0.0;
    public double speed = 0.0;
    public double powerLevel = 0.0;

    private static List<Class<? extends BaseMelee>> ver = new ArrayList<>();

    public static void init(){
        ver.add(Sword.class);
        ver.add(Axe.class);
        ver.add(Spear.class);
    }

    public static boolean allowSlot(Slot s){
        return s == Slot.BOTH_HANDS || s == Slot.MAIN_HAND || s == Slot.OFF_HAND;
    }
    public abstract ItemStack toItem();
    public abstract void active(PlayerInteractEvent e);

    public abstract boolean fromItem(ItemStack itemStack);

    public static <T extends  BaseMelee> T getNewInstance(ItemStack itemStack, Class<T> type){
        T r = null;
        try {
            Constructor<T> con = type.getConstructor(ItemStack.class);
            r = (T) con.newInstance((Object) itemStack);
        }catch (Exception e){
            e.printStackTrace();
        }
        if (r == null){
            System.out.println(type.getName() + " Doesn't have Constructor for ItemStack");
        }
        return r;
    }

    public static boolean isBaseMelee(ItemStack itemStack){
        for (Class<? extends BaseMelee> c : ver) {
            try {
                if ((boolean) c.getMethod("fromItem", ItemStack.class).invoke(getNewInstance(itemStack, c),itemStack)) {
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static Class<? extends BaseMelee> getType(ItemStack itemStack){
        for (Class<? extends BaseMelee> c : ver) {
            try {
                if ((boolean) c.getMethod("fromItem", ItemStack.class).invoke(getNewInstance(itemStack, c),itemStack)) {
                    return c;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public double getDamage(){
        Random r = new Random();
        if (r.nextInt(5) >= 4 ){
            return damage * crit;
        }
        return damage;
    }
}
