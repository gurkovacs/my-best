package com.fireages.combat.items;

import com.fireages.combat.data.PlayerData;
import com.fireages.combat.raycast.Utils;
import de.tr7zw.nbtapi.*;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class Gun extends BaseRange {

    private long stage = 0;
    private long lastClick = 0;
    public int magazineSize = 0;
    public int magazine = 0;

    public Gun(ItemStack is){
        this.fromItem(is);
    }

    public Gun(double damage, Slot slot, double crit, double range, double speed, int magazineSize, double power){
        this.damage  = damage;
        this.crit = crit;
        this.range = range;
        this.speed = speed;
        this.magazineSize = magazineSize;
        this.magazine = 0;
        this.powerLevel = power;
        if(allowSlot(slot)) {
            this.slot = slot;
        }
        else {
            this.slot = Slot.MAIN_HAND;
        }
    }
    @Override
    public ItemStack toItem() {
        ItemStack is = new ItemStack(Material.WOODEN_HOE);
        ItemMeta im = is.getItemMeta();
        im.setUnbreakable(true);
        if (slot == Slot.MAIN_HAND){
            im.setDisplayName(ChatColor.RESET + "Heavy Pistol");
        }else if (slot == Slot.OFF_HAND){
            im.setDisplayName(ChatColor.RESET + "Light Pistol");
        }else if (slot == Slot.BOTH_HANDS){
            im.setDisplayName(ChatColor.RESET + "Rifle");
        }
        List<String> lore = new ArrayList<>();
        lore.add("");
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Damage " + ChatColor.GRAY + damage);
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Critical Modifier " + ChatColor.GRAY + crit);
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Range " + ChatColor.GRAY + range);
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Speed " + ChatColor.GRAY + speed);
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Magazine Size " + ChatColor.GRAY + magazineSize);
        im.setLore(lore);
        im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE);
        is.setItemMeta(im);
        NBTItem nbti = new NBTItem(is);
        nbti.setLong("Stage", stage);
        nbti.setInteger("Magazine", magazine);
        nbti.setLong("LastClick", lastClick);
        nbti.setDouble("PowerLevel", powerLevel);
        NBTCompoundList attribute = nbti.getCompoundList("AttributeModifiers");
        NBTListCompound mod1 = attribute.addCompound();
        mod1.setInteger("Amount", Integer.MAX_VALUE);
        mod1.setString("AttributeName", "generic.attackSpeed");
        mod1.setString("Name", "generic.attackSpeed");
        mod1.setInteger("Operation", 0);
        mod1.setInteger("UUIDLeast", 59664);
        mod1.setInteger("UUIDMost", 31453);
        is = nbti.getItem();
        return is;
    }

    @Override
    public ItemStack shoot(PlayerInteractEvent e, ItemStack item) {
        if(magazine > 0 && System.currentTimeMillis() - lastClick > 500){
            Location loc = e.getPlayer().getEyeLocation();
            Location c = loc.clone();
            Vector dir = loc.getDirection();
            double acc = 0.1;

            for(double i = 0; loc.distance(c) < range; i += acc){
                c.add(dir.clone().multiply(i));
                c.getWorld().spawnParticle(Particle.SMOKE_NORMAL,c, 5, 0,0,0,0);
                for(Entity en : loc.getWorld().getNearbyEntities(c, acc, acc, acc)){
                    double tempd = this.getDamage();
                    if(en.getType() == EntityType.PLAYER && !en.getUniqueId().equals(e.getPlayer().getUniqueId())){
                        Player d = (Player) en;
                        PlayerData data = PlayerData.getDataForPlayer(d);
                        data.damage(tempd, tempd > damage, d.getEyeLocation());
                        data.update(d);
                        Vector v = Utils.genVec(e.getPlayer().getLocation(), d.getLocation());
                        v.add(new Vector(0, 1, 0));
                        d.setVelocity(v.multiply(0.5));
                        break;
                    }
                }
                if(c.getBlock().getType().isSolid()){
                    break;
                }
            }
            magazine -= 1;
        }else {
            if (System.currentTimeMillis() - lastClick > 500){
                stage = 0;
                lastClick = System.currentTimeMillis();
            }
            if (stage > speed * 1000 && magazine == 0){
                e.getPlayer().sendMessage("Loaded!");
                magazine = magazineSize;
            }
            stage += (System.currentTimeMillis() - lastClick);
            lastClick = System.currentTimeMillis();
        }
        NBTItem nbti = new NBTItem(item);
        nbti.setInteger("Magazine", magazine);
        nbti.setLong("Stage", stage);
        nbti.setLong("LastClick", lastClick);
        return nbti.getItem();
    }

    @Override
    public boolean fromItem(ItemStack itemStack) {
        if(itemStack == null || itemStack.getType() != Material.WOODEN_HOE){
            return false;
        }
        try {
            String name = ChatColor.stripColor(itemStack.getItemMeta().getDisplayName());
            if (name.equals("Heavy Pistol")) {
                this.slot = Slot.MAIN_HAND;
            } else if (name.equals("Light Pistol")) {
                this.slot = Slot.OFF_HAND;
            } else if (name.equals("Rifle")) {
                this.slot = Slot.BOTH_HANDS;
            } else {
                return false;
            }
            String s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(1));
            damage = Double.parseDouble(s.replaceAll(ONLY_NUMBER, ""));
            s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(2));
            crit = Double.parseDouble(s.replaceAll(ONLY_NUMBER, ""));
            s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(3));
            range = Double.parseDouble(s.replaceAll(ONLY_NUMBER, ""));
            s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(4));
            speed = Double.parseDouble(s.replaceAll(ONLY_NUMBER, ""));
            s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(5));
            magazineSize = Integer.parseInt(s.replaceAll(ONLY_NUMBER, ""));
            NBTItem nbti = new NBTItem(itemStack);
            magazine = nbti.getInteger("Magazine");
            stage = nbti.getLong("Stage");
            lastClick = nbti.getLong("LastClick");
            powerLevel = nbti.getDouble("PowerLevel");
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public double getPowerLevel() {
        return powerLevel;
    }

    public static ItemStack getIcon(){
        ItemStack is = new ItemStack(Material.WOODEN_HOE);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName("Gun");
        is.setItemMeta(im);
        return is;
    }
}
