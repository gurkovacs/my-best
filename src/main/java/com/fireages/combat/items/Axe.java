package com.fireages.combat.items;

import com.fireages.combat.data.PlayerData;
import com.fireages.combat.raycast.Utils;
import de.tr7zw.nbtapi.NBTCompoundList;
import de.tr7zw.nbtapi.NBTItem;
import de.tr7zw.nbtapi.NBTListCompound;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Axe extends BaseMelee {

    public Axe(ItemStack is){
        this.fromItem(is);
    }

    public Axe(double damage, Slot slot, double crit, double range, double speed, double power){
        this.damage  = damage;
        this.crit = crit;
        this.range = range;
        this.speed = speed;
        this.powerLevel = power;
        if(allowSlot(slot)) {
            this.slot = slot;
        }
        else {
            this.slot = Slot.MAIN_HAND;
        }
    }

    @Override
    public ItemStack toItem() {
        ItemStack is = new ItemStack(Material.STONE_AXE);
        ItemMeta im = is.getItemMeta();
        im.setUnbreakable(true);
        if (slot == Slot.MAIN_HAND){
            im.setDisplayName(ChatColor.RESET + "War Axe");
        }else if (slot == Slot.OFF_HAND){
            im.setDisplayName(ChatColor.RESET + "Hand Axe");
        }else if (slot == Slot.BOTH_HANDS){
            im.setDisplayName(ChatColor.RESET + "Great Axe");
        }
        List<String> lore = new ArrayList<>();
        lore.add("");
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Damage " + ChatColor.GRAY + damage);
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Critical Modifier " + ChatColor.GRAY + crit);
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Range " + ChatColor.GRAY + range);
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Speed " + ChatColor.GRAY + speed);
        im.setLore(lore);
        im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE);
        is.setItemMeta(im);
        NBTItem nbti = new NBTItem(is);
        NBTCompoundList attribute = nbti.getCompoundList("AttributeModifiers");
        NBTListCompound mod1 = attribute.addCompound();
        mod1.setInteger("Amount", Integer.MAX_VALUE);
        mod1.setString("AttributeName", "generic.attackSpeed");
        mod1.setString("Name", "generic.attackSpeed");
        mod1.setInteger("Operation", 0);
        mod1.setInteger("UUIDLeast", 59664);
        mod1.setInteger("UUIDMost", 31453);
        nbti.setDouble("PowerLevel", powerLevel);
        is = nbti.getItem();
        return is;
    }

    @Override
    public void active (PlayerInteractEvent e) {
        Player p = e.getPlayer();
        Entity de = Utils.getFirst(p.getEyeLocation(), 0.1, range, EntityType.PLAYER, p);
        if(de instanceof  Player){
            Player d = (Player) de;
            ItemStack is = d.getInventory().getItemInMainHand();
            p.getWorld().dropItem(d.getEyeLocation(), is);
            d.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
        }
    }

    @Override
    public boolean fromItem(ItemStack itemStack) {
        if(itemStack == null || itemStack.getType() != Material.STONE_AXE){
            return false;
        }
        try {
            String name = ChatColor.stripColor(itemStack.getItemMeta().getDisplayName());
            if (name.equals("War Axe")) {
                this.slot = Slot.MAIN_HAND;
            } else if (name.equals("Hand Axe")) {
                this.slot = Slot.OFF_HAND;
            } else if (name.equals("Great Axe")) {
                this.slot = Slot.BOTH_HANDS;
            } else {
                return false;
            }
            String s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(1));
            damage = Double.parseDouble(s.replaceAll(ONLY_NUMBER, ""));
            s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(2));
            crit = Double.parseDouble(s.replaceAll(ONLY_NUMBER, ""));
            s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(3));
            range = Double.parseDouble(s.replaceAll(ONLY_NUMBER, ""));
            s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(4));
            speed = Double.parseDouble(s.replaceAll(ONLY_NUMBER, ""));
            NBTItem nbti = new NBTItem(itemStack);
            powerLevel = nbti.getDouble("PowerLevel");
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
    @Override
    public double getPowerLevel() {
        return powerLevel;
    }
}
