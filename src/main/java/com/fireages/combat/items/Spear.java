package com.fireages.combat.items;

import com.fireages.combat.raycast.Utils;
import de.tr7zw.nbtapi.NBTCompoundList;
import de.tr7zw.nbtapi.NBTItem;
import de.tr7zw.nbtapi.NBTListCompound;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class Spear extends BaseMelee {

    public Spear(ItemStack is){
        this.fromItem(is);
    }

    public Spear(double damage, Slot slot, double crit, double range, double speed, double power){
        this.damage  = damage;
        this.crit = crit;
        this.range = range;
        this.speed = speed;
        this.powerLevel = power;
        if(allowSlot(slot)) {
            this.slot = slot;
        }
        else {
            this.slot = Slot.MAIN_HAND;
        }
    }

    @Override
    public ItemStack toItem() {
        ItemStack is = new ItemStack(Material.STICK);
        ItemMeta im = is.getItemMeta();
        im.setUnbreakable(true);
        if (slot == Slot.MAIN_HAND){
            im.setDisplayName(ChatColor.RESET + "Spear");
        }else if (slot == Slot.OFF_HAND){
            im.setDisplayName(ChatColor.RESET + "Light Spear");
        }else if (slot == Slot.BOTH_HANDS){
            im.setDisplayName(ChatColor.RESET + "Heavy Spear");
        }
        List<String> lore = new ArrayList<>();
        lore.add("");
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Damage " + ChatColor.GRAY + damage);
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Critical Modifier " + ChatColor.GRAY + crit);
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Range " + ChatColor.GRAY + range);
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Speed " + ChatColor.GRAY + speed);
        im.setLore(lore);
        im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE);
        is.setItemMeta(im);
        NBTItem nbti = new NBTItem(is);
        NBTCompoundList attribute = nbti.getCompoundList("AttributeModifiers");
        NBTListCompound mod1 = attribute.addCompound();
        mod1.setInteger("Amount", Integer.MAX_VALUE);
        mod1.setString("AttributeName", "generic.attackSpeed");
        mod1.setString("Name", "generic.attackSpeed");
        mod1.setInteger("Operation", 0);
        mod1.setInteger("UUIDLeast", 59664);
        mod1.setInteger("UUIDMost", 31453);
        nbti.setDouble("PowerLevel", powerLevel);
        is = nbti.getItem();
        return is;
    }

    @Override
    public void active (PlayerInteractEvent e) {
        Player p = e.getPlayer();
        p.setVelocity(new Vector(0,2.5,0));
    }

    @Override
    public boolean fromItem(ItemStack itemStack) {
        if(itemStack == null || itemStack.getType() != Material.STICK){
            return false;
        }
        try {
            String name = ChatColor.stripColor(itemStack.getItemMeta().getDisplayName());
            if (name.equals("Spear")) {
                this.slot = Slot.MAIN_HAND;
            } else if (name.equals("Light Spear")) {
                this.slot = Slot.OFF_HAND;
            } else if (name.equals("Heavy Spear")) {
                this.slot = Slot.BOTH_HANDS;
            } else {
                return false;
            }
            String s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(1));
            damage = Double.parseDouble(s.replaceAll(ONLY_NUMBER, ""));
            s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(2));
            crit = Double.parseDouble(s.replaceAll(ONLY_NUMBER, ""));
            s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(3));
            range = Double.parseDouble(s.replaceAll(ONLY_NUMBER, ""));
            s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(4));
            speed = Double.parseDouble(s.replaceAll(ONLY_NUMBER, ""));
            NBTItem nbti = new NBTItem(itemStack);
            powerLevel = nbti.getDouble("PowerLevel");
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
    @Override
    public double getPowerLevel() {
        return powerLevel;
    }
}
