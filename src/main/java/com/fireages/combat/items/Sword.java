package com.fireages.combat.items;

import com.fireages.combat.data.PlayerData;
import com.fireages.combat.raycast.Utils;
import de.tr7zw.nbtapi.NBTCompoundList;
import de.tr7zw.nbtapi.NBTItem;
import de.tr7zw.nbtapi.NBTListCompound;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Sword extends BaseMelee {

    public Sword(ItemStack is){
        this.fromItem(is);
    }

    public Sword(double damage, Slot slot, double crit,double range, double speed, double power){
        this.damage  = damage;
        this.crit = crit;
        this.range = range;
        this.speed = speed;
        this.powerLevel = power;
        if(allowSlot(slot)) {
            this.slot = slot;
        }
        else {
            this.slot = Slot.MAIN_HAND;
        }
    }

    @Override
    public ItemStack toItem() {
        ItemStack is = new ItemStack(Material.IRON_SWORD);
        ItemMeta im = is.getItemMeta();
        im.setUnbreakable(true);
        if (slot == Slot.MAIN_HAND){
            im.setDisplayName(ChatColor.RESET + "Long Sword");
        }else if (slot == Slot.OFF_HAND){
            im.setDisplayName(ChatColor.RESET + "Short Sword");
        }else if (slot == Slot.BOTH_HANDS){
            im.setDisplayName(ChatColor.RESET + "Great Sword");
        }
        List<String> lore = new ArrayList<>();
        lore.add("");
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Damage " + ChatColor.GRAY + damage);
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Critical Modifier " + ChatColor.GRAY + crit);
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Range " + ChatColor.GRAY + range);
        lore.add(ChatColor.RESET + "" + ChatColor.YELLOW + "Speed " + ChatColor.GRAY + speed);
        im.setLore(lore);
        im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE);
        is.setItemMeta(im);
        NBTItem nbti = new NBTItem(is);
        NBTCompoundList attribute = nbti.getCompoundList("AttributeModifiers");
        NBTListCompound mod1 = attribute.addCompound();
        mod1.setInteger("Amount", Integer.MAX_VALUE);
        mod1.setString("AttributeName", "generic.attackSpeed");
        mod1.setString("Name", "generic.attackSpeed");
        mod1.setInteger("Operation", 0);
        mod1.setInteger("UUIDLeast", 59664);
        mod1.setInteger("UUIDMost", 31453);
        nbti.setDouble("PowerLevel", powerLevel);
        is = nbti.getItem();
        return is;
    }

    @Override
    public void active (PlayerInteractEvent e) {
        Player p = e.getPlayer();
        Location loc = p.getLocation();
        for(Entity en : loc.getWorld().getNearbyEntities(loc,range * 1.5,range * 1.5,range * 1.5)){
            if(en instanceof Player && !((Player) en).getUniqueId().equals(p.getUniqueId())){
                Player d = (Player) en;
                PlayerData pdd = PlayerData.getDataForPlayer(d);
                double dam = getDamage();
                pdd.damage(dam, dam > damage, d.getEyeLocation());
                d.setVelocity(Utils.genVec(loc, d.getLocation()));
                pdd.update(d);
            }
        }
    }

    @Override
    public boolean fromItem(ItemStack itemStack) {
        if(itemStack == null || itemStack.getType() != Material.IRON_SWORD){
            return false;
        }
        try {
            String name = ChatColor.stripColor(itemStack.getItemMeta().getDisplayName());
            if (name.equals("Long Sword")) {
                this.slot = Slot.MAIN_HAND;
            } else if (name.equals("Short Sword")) {
                this.slot = Slot.OFF_HAND;
            } else if (name.equals("Great Sword")) {
                this.slot = Slot.BOTH_HANDS;
            } else {
                return false;
            }
            String s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(1));
            damage = Double.parseDouble(s.replaceAll(ONLY_NUMBER, ""));
            s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(2));
            crit = Double.parseDouble(s.replaceAll(ONLY_NUMBER, ""));
            s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(3));
            range = Double.parseDouble(s.replaceAll(ONLY_NUMBER, ""));
            s = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(4));
            speed = Double.parseDouble(s.replaceAll(ONLY_NUMBER, ""));
            NBTItem nbti = new NBTItem(itemStack);
            powerLevel = nbti.getDouble("PowerLevel");
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
    @Override
    public double getPowerLevel() {
        return powerLevel;
    }
}
