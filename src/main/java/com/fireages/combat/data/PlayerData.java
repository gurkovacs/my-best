package com.fireages.combat.data;

import com.fireages.combat.Combat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class PlayerData {

    public final static File DIR = new File(Combat.instance.getDataFolder(),"players");
    public File file;
    public YamlConfiguration yaml;

    public static HashMap<UUID, PlayerData> cashe = new HashMap<>();

    public double maxHealth = 250;
    public UUID uuid;
    public double health = maxHealth;
    public int points;

    public static PlayerData getDataForPlayer(OfflinePlayer p){
        if(cashe.containsKey(p.getUniqueId())){
            return cashe.get(p.getUniqueId());
        }
        return new PlayerData(p.getUniqueId());
    }
    private PlayerData(UUID uuid){
        this.uuid = uuid;
        if(!DIR.exists()){
            DIR.mkdirs();
        }
        file = new File(DIR,uuid.toString() + ".yaml");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch(IOException e) {
                e.printStackTrace();
            }
            yaml = new YamlConfiguration();
        } else {
            load();
        }
        save();
        cashe.put(uuid, this);
    }

    public void save(){
        yaml.set("health", health);
        yaml.set("max-health", maxHealth);
        yaml.set("points", points);
        try {
            yaml.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load(){
        yaml = new YamlConfiguration();
        try {
            yaml.load(file);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }

        health = yaml.getDouble("health");
        maxHealth = yaml.getDouble("max-health");
        points = yaml.getInt("points");

    }

    public void update(Player p){
        double scale = health / maxHealth * 20.0;
        p.setHealthScale(20.0);
        p.setHealth(scale);
    }

    public void respawn(Player p){
        health = maxHealth;
        Random r = new Random();

        update(p);
    }
    public void damage(double d, boolean isCrit, Location loc){
        health -= d;
        if (health <= 0) {
            health = 0;
        }
        String str = "❤" + String.format("%.2f", d);
        if(isCrit){
            str = ChatColor.YELLOW + "" + ChatColor.BOLD + str;
        }else {
            str = ChatColor.RED + "" + ChatColor.BOLD + str;
        }
        save();
        Random r = new Random();
        ArmorStand ar = loc.getWorld().spawn(loc, ArmorStand.class);
        ar.setCustomName(str);
        ar.setSmall(true);
        ar.setVisible(false);
        ar.setCustomNameVisible(true);
        ar.setVelocity(new Vector(r.nextDouble() * 0.25, r.nextDouble() * 0.25, r.nextDouble() * 0.25));
        ar.setInvulnerable(true);
        Bukkit.getScheduler().runTaskLater(Combat.instance,() -> {ar.remove();},25);
        
    }
}
