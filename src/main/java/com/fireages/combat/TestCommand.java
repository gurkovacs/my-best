package com.fireages.combat;

import com.fireages.combat.data.PlayerData;
import com.fireages.combat.items.Axe;
import com.fireages.combat.items.Gun;
import com.fireages.combat.items.Slot;
import com.fireages.combat.items.Sword;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TestCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player){
            Player p = (Player) sender;
            Sword s1 = new Sword(10.0, Slot.OFF_HAND, 2.5, 3.4, 0.2, 15);
            Sword s2 = new Sword(15.0, Slot.MAIN_HAND, 1.5, 3.4, 0.5, 15);
            Sword s3 = new Sword(25.0, Slot.BOTH_HANDS, 1.5,3.4, 0.4, 15);
            Axe a1 = new Axe(10.0, Slot.OFF_HAND, 2.5, 3.4, 0.2, 15);
            Axe a2 = new Axe(15.0, Slot.MAIN_HAND, 1.5, 3.4, 0.5, 15);
            Axe a3 = new Axe(25.0, Slot.BOTH_HANDS, 1.5,3.4, 0.4, 15);
            Axe sp1 = new Axe(10.0, Slot.OFF_HAND, 2.5, 3.4, 0.2, 15);
            Axe sp2 = new Axe(15.0, Slot.MAIN_HAND, 1.5, 3.4, 0.5, 15);
            Axe sp3 = new Axe(25.0, Slot.BOTH_HANDS, 1.5,3.4, 0.4, 15);
            Gun g1 = new Gun(10.0, Slot.OFF_HAND, 2.5, 16, 3, 8, 15);
            Gun g2 = new Gun(15.0, Slot.MAIN_HAND, 1.5, 16, 3.5,6, 15);
            Gun g3 = new Gun(25.0, Slot.BOTH_HANDS, 2.5,32.0, 5, 1, 15);
            p.getInventory().addItem(s1.toItem(), s2.toItem(), s3.toItem(), g1.toItem(), g2.toItem(), g3.toItem(), a1.toItem(), a2.toItem(), a3.toItem());
            p.sendMessage("you got test Items!");


            PlayerData pd = PlayerData.getDataForPlayer(p);
            pd.health = pd.maxHealth;
            pd.save();
            pd.update(p);
        }
        return false;
    }
}
