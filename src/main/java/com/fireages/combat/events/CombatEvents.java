package com.fireages.combat.events;

import com.fireages.combat.data.PlayerData;
import com.fireages.combat.items.BaseMelee;
import com.fireages.combat.items.BaseRange;
import com.fireages.combat.items.Gun;
import com.fireages.combat.items.Slot;
import com.fireages.combat.raycast.Utils;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.UUID;

public class CombatEvents implements Listener {

    private static HashMap<UUID, Long> lastAttack = new HashMap<>();
    private static HashMap<UUID, Long> lastAction = new HashMap<>();



    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent e) {
        e.setFoodLevel(20);
        e.setCancelled(true);
    }

    @EventHandler
    public void onEntityRegainHealth(EntityRegainHealthEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerItemHeld(PlayerItemHeldEvent e) {
        ItemStack main = e.getPlayer().getInventory().getItem(e.getNewSlot());
        Slot slot = Slot.OFF_HAND;
        if (BaseMelee.isBaseMelee(main)) {
            Class<? extends BaseMelee> mainClass = BaseMelee.getType(main);
            if (mainClass != null) {
                BaseMelee mainMelee = BaseMelee.getNewInstance(main, mainClass);
                slot = mainMelee.slot;
            }
        }else if (BaseRange.isBaseRange(main)) {
            Class<? extends BaseRange> mainClass = BaseRange.getType(main);
            if (mainClass != null) {
                BaseRange mainMelee = BaseRange.getNewInstance(main, mainClass);
                slot = mainMelee.slot;
            }
        }
        if (slot == Slot.BOTH_HANDS && !(e.getPlayer().getInventory().getItemInOffHand().getType() == Material.AIR)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        ItemStack main = e.getWhoClicked().getInventory().getItemInMainHand();
        Slot slotMain = Slot.OFF_HAND;
        Slot slotOff = Slot.OFF_HAND;
        if (e.getSlotType() == InventoryType.SlotType.QUICKBAR && (e.getSlot() == 40 || e.getSlot() < 9) &&
                (e.getAction() == InventoryAction.PLACE_ALL ||
                        e.getAction() == InventoryAction.PLACE_ONE ||
                        e.getAction() == InventoryAction.PLACE_SOME ||
                        e.getAction() == InventoryAction.SWAP_WITH_CURSOR ||
                        e.getAction() == InventoryAction.HOTBAR_SWAP)) {
            ItemStack off = e.getCursor();
            if (BaseMelee.isBaseMelee(main) || BaseMelee.isBaseMelee(off) ) {
                Class<? extends BaseMelee> offClass = BaseMelee.getType(off);
                Class<? extends BaseMelee> mainClass = BaseMelee.getType(main);
                if (mainClass != null) {
                    BaseMelee mainMelee = BaseMelee.getNewInstance(main, mainClass);
                    slotMain = mainMelee.slot;
                }
                if (offClass != null) {
                    BaseMelee offMelee = BaseMelee.getNewInstance(off, offClass);
                    slotOff = offMelee.slot;
                }

            }
            if (BaseRange.isBaseRange(main) || BaseRange.isBaseRange(off)) {
                Class<? extends BaseRange> offClass = BaseRange.getType(off);
                Class<? extends BaseRange> mainClass = BaseRange.getType(main);
                if (mainClass != null) {
                    BaseRange mainMelee = BaseRange.getNewInstance(main, mainClass);
                    slotMain = mainMelee.slot;
                }
                if (offClass != null) {
                    BaseRange offMelee = BaseRange.getNewInstance(off, offClass);
                    slotOff = offMelee.slot;
                }

            }
            if ((slotMain == Slot.BOTH_HANDS || slotOff != Slot.OFF_HAND) && e.getSlot() == 40){
                e.setCancelled(true);
            }
            if (slotOff == Slot.BOTH_HANDS && e.getSlot() < 9) {
                if (!(e.getWhoClicked().getInventory().getItemInOffHand().getType() == Material.AIR)) {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerSwapHandItems(PlayerSwapHandItemsEvent e) {
        ItemStack main = e.getOffHandItem();
        Slot slot = Slot.OFF_HAND;
        if (BaseMelee.isBaseMelee(main)) {
            Class<? extends BaseMelee> mainClass = BaseMelee.getType(main);
            if (mainClass != null) {
                BaseMelee mainMelee = BaseMelee.getNewInstance(main, mainClass);
                slot = mainMelee.slot;
            }
        }else if (BaseRange.isBaseRange(main)) {
            Class<? extends BaseRange> mainClass = BaseRange.getType(main);
            if (mainClass != null) {
                BaseRange mainMelee = BaseRange.getNewInstance(main, mainClass);
                slot = mainMelee.slot;
            }
        }
        if (slot != Slot.OFF_HAND) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            e.setDamage(0.0);
            Player d = (Player) e.getEntity();
            PlayerData data = PlayerData.getDataForPlayer(d);
            data.update(d);
            e.setCancelled(true);
        }
    }


    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        PlayerData data = PlayerData.getDataForPlayer(e.getPlayer());
        data.respawn(e.getPlayer());
    }

    @EventHandler
    public void onEntityDamageByEntity(PlayerInteractEvent e) {
        Player a = e.getPlayer();
        if (e.getAction() == Action.LEFT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_AIR) {
            double finalDamage = 0;
            double baseDamage = 0;
            Entity de = null;
            double speed = 0;
            if (BaseMelee.isBaseMelee(a.getInventory().getItemInMainHand()) ||
                    BaseMelee.isBaseMelee(a.getInventory().getItemInOffHand())) {
                Class<? extends BaseMelee> offClass = BaseMelee.getType(a.getInventory().getItemInOffHand());
                Class<? extends BaseMelee> mainClass = BaseMelee.getType(a.getInventory().getItemInMainHand());
                double range = 0;
                if (mainClass != null) {
                    BaseMelee mainMelee = BaseMelee.getNewInstance(a.getInventory().getItemInMainHand(), mainClass);
                    finalDamage = mainMelee.getDamage();
                    baseDamage = mainMelee.damage;
                    range = mainMelee.range;
                    speed = mainMelee.speed;
                }
                if (offClass != null) {
                    BaseMelee offMelee = BaseMelee.getNewInstance(a.getInventory().getItemInOffHand(), offClass);
                    finalDamage += offMelee.getDamage();
                    baseDamage += offMelee.damage;
                    speed = Math.max(offMelee.speed, speed);
                    range = Math.max(offMelee.range, range);
                }
                de = Utils.getFirst(a.getEyeLocation(), 0.1, range, EntityType.PLAYER, a);
            }
            if(de instanceof Player){
                Player d = (Player) de;
                if (!d.getUniqueId().equals(a.getUniqueId()) && d.getGameMode() != GameMode.CREATIVE &&
                        ((lastAttack.containsKey(a.getUniqueId()) && System.currentTimeMillis() - lastAttack.get(a.getUniqueId()) > speed * 1000) ||
                                !lastAttack.containsKey(a.getUniqueId()))) {
                    PlayerData data = PlayerData.getDataForPlayer(d);
                    data.damage(finalDamage, finalDamage > baseDamage, d.getEyeLocation());
                    data.update(d);
                    Vector v = Utils.genVec(a.getLocation(), d.getLocation());
                    if (d.isOnGround()) {
                        v.add(new Vector(0, 1, 0));
                    }
                    if (a.getVelocity().getY() < 0) {
                        v.multiply(0.85);
                    }

                    d.setVelocity(v.multiply(0.5));
                    lastAttack.put(a.getUniqueId(), System.currentTimeMillis());
                }
            }
        } else if (e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_AIR) {

            if (BaseMelee.isBaseMelee(a.getInventory().getItemInMainHand())) {
                Class<? extends BaseMelee> mainClass = BaseMelee.getType(a.getInventory().getItemInMainHand());
                if (mainClass != null) {
                    BaseMelee mainMelee = BaseMelee.getNewInstance(a.getInventory().getItemInMainHand(), mainClass);
                    if (((lastAction.containsKey(a.getUniqueId()) && System.currentTimeMillis() - lastAction.get(a.getUniqueId()) > mainMelee.speed * 15000) ||
                            !lastAction.containsKey(a.getUniqueId()))) {
                        mainMelee.active(e);
                        lastAction.put(a.getUniqueId(), System.currentTimeMillis());
                    } else {
                        a.sendMessage(ChatColor.RED + "Time remanding: " + ((System.currentTimeMillis() - lastAction.get(a.getUniqueId())) / 1000) + "/" + (int) (mainMelee.speed *10));
                    }
                }
                e.setCancelled(true);
            }
            if (BaseRange.isBaseRange(a.getInventory().getItemInMainHand()) || BaseRange.isBaseRange(a.getInventory().getItemInOffHand())) {

                Class<? extends BaseRange> offClass = BaseRange.getType(a.getInventory().getItemInOffHand());
                Class<? extends BaseRange> mainClass = BaseRange.getType(a.getInventory().getItemInMainHand());
                if (mainClass != null) {
                    BaseRange mainRange = BaseRange.getNewInstance(a.getInventory().getItemInMainHand(), mainClass);
                    if(a.isSneaking() && mainRange instanceof Gun){
                        a.sendMessage(ChatColor.DARK_BLUE + "Ammo left: " + ((Gun)mainRange).magazine);
                    }else {
                        a.getInventory().setItemInMainHand(mainRange.shoot(e, a.getInventory().getItemInMainHand()));
                    }
                }
                if (offClass != null) {
                    BaseRange offRange = BaseRange.getNewInstance(a.getInventory().getItemInOffHand(), offClass);
                    if(a.isSneaking() && offRange instanceof Gun){
                        a.sendMessage(ChatColor.DARK_BLUE + "Ammo left: " + ((Gun)offRange).magazine);
                    }else {
                        a.getInventory().setItemInOffHand(offRange.shoot(e, a.getInventory().getItemInOffHand()));
                    }
                }
                e.setCancelled(true);
            }
        }
    }


    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        PlayerData data = PlayerData.getDataForPlayer(e.getPlayer());
        data.update(e.getPlayer());
    }
}
